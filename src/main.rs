
use std::collections::HashMap;

use bevy::{app::ScheduleRunnerPlugin, log::LogPlugin, prelude::*};
use bevy_quinnet::{
    server::{
        certificate::CertificateRetrievalMode, ConnectionLostEvent, Endpoint, QuinnetServerPlugin,
        Server, ServerConfigurationData,
    },
    shared::ClientId,
};
use ed25519_dalek::PublicKey;
use tabletop_nostr::message::ClientMessage;

#[derive(Resource, Debug, Clone, Default)]
struct Users {
    keys: HashMap<ClientId, PublicKey>,
    client_ids: HashMap<PublicKey, ClientId>
}

fn handle_client_messages(mut server: ResMut<Server>, mut users: ResMut<Users>) {
    let endpoint = server.endpoint_mut();

    loop {
        let message_result = endpoint.receive_message::<String>();
        if !message_result.is_ok(){
            dbg!(message_result);
            break;
        };
        let message_option = message_result.unwrap();
        if message_option.is_none(){
            //dbg!("No Message");
            break;
        }
        dbg!(message_option.unwrap().0);
    }
    /*
    while let Ok(Some((message, client_id))) = endpoint.receive_message::<ClientMessage>() {
        dbg!(message);
    }*/
}

fn handle_server_events(
    mut connection_lost_events: EventReader<ConnectionLostEvent>,
    mut server: ResMut<Server>,
    mut users: ResMut<Users>,
) {
    // The server signals us about users that lost connection
    for client in connection_lost_events.iter() {
        handle_disconnect(server.endpoint_mut(), &mut users, client.id);
    }
}

/// Shared disconnection behaviour, whether the client lost connection or asked to disconnect
fn handle_disconnect(endpoint: &mut Endpoint, users: &mut ResMut<Users>, client_id: ClientId) {

}

fn start_listening(mut server: ResMut<Server>) {
    server
        .start_endpoint(
            ServerConfigurationData::new("127.0.0.1".to_string(), 6000, "0.0.0.0".to_string()),
            CertificateRetrievalMode::GenerateSelfSigned,
        )
        .unwrap();
}

fn main() {
    App::new()
        .add_plugin(ScheduleRunnerPlugin::default())
        .add_plugin(LogPlugin::default())
        .add_plugin(QuinnetServerPlugin::default())
        .insert_resource(Users::default())
        .add_startup_system(start_listening)
        .add_system(handle_client_messages)
        .add_system(handle_server_events)
        .run();
}
